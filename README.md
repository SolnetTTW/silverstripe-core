# Solnet SilverStripe Core Environment

## Solnet team details

Tech lead: Darren Inwood (darren.inwood@solnet.co.nz)

## Overview of the project

Includes environment configuration details for running a SilverStripe project inside Solnet's
Docker-based hosting environments.

## Email configuration

When the project's environment type is "dev", emails are configured to be sent to 
''mailcatcher:1025'' with TLS disabled.

This will deliver emails to the ''mailcatcher'' container provided by the ''docker-ss''
command, which is installed by the ''solnet/silverstripe-docker'' global composer package.

## Log configuration

When the project has environment type "dev", logs are configured to send directly to 
Elasticsearch via the Gelf transport method.

For non-dev environment types, no logging is configured but errors are not output to the screen.

**Note** PHP errors that happen outside of SilverStripe do not make it into the SilverStripe
logging system.  If you are using requests that don't go through the usual SilverStripe routing,
eg. direct access to ''.php'' scripts, any errors won't be logged without further effort.
